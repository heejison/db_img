import mariadb
import base64
from io import BytesIO
import time

connection = mariadb.connect(host='localhost',
                             user='root',
                             password='1014',
                             database='kiosk')

# 이미지 파일 오픈
with open('C:/Users/pc/PycharmProjects/Kiosk__/Image/딸기스무디.PNG',"rb") as image_file:
    binary_image = image_file.read()

#인코딩
binary_image = base64.b64encode(binary_image)

# utf-8 방식으로 다시 디코딩
binary_image = binary_image.decode('UTF-8')
print(binary_image)

#sql
sql = "insert into image (name, image_data) values ('{}','{}') ".format('딸기스무디', binary_image)
print(sql)
# connect 에서 cursor 생성
cursors = connection.cursor()
cursors.execute(sql)
connection.commit()
connection.close()